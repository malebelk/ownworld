package com.malebelk.ownworld;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class TreeItemNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(TreeItemNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String treeItemNotFoundHandler(TreeItemNotFoundException ex) {
        return ex.getMessage();
    }
}
