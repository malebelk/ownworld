package com.malebelk.ownworld;

import org.springframework.web.bind.annotation.*;

import java.util.Set;


@RestController
public class TreeItemController {
    private final TreeItemRepository repository;

    TreeItemController(TreeItemRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/api/treeitem/")
    TreeItem getRoot() {
        return repository.getOneByParentNull();
    }

    @PostMapping("/api/treeitem")
    TreeItem newTreeItem(@RequestBody TreeItem newTreeItem) {
        return repository.save(newTreeItem);
    }

    @GetMapping("/api/treeitem/{id}")
    TreeItem getTreeItem(@PathVariable Integer id) throws TreeItemNotFoundException {
        return repository.findById(id)
                .orElseThrow(() -> new TreeItemNotFoundException(id));
    }

    @GetMapping("/api/treeitem/childof/{id}")
    Set<TreeItem> getTreeItemChildren(@PathVariable Integer id) throws TreeItemNotFoundException {
        return repository.findByParentId(id);
    }

    @PutMapping("/api/treeitem/{id}")
    TreeItem replaceTreeItem(@RequestBody TreeItem newTreeItem, @PathVariable Integer id) {
        return repository.findById(id)
                .map(treeItem -> {
                    treeItem.setName(newTreeItem.getName());
                    treeItem.setPicture(newTreeItem.getPicture());
                    treeItem.setContent(newTreeItem.getContent());
                    //treeItem.setChildren(newTreeItem.getChildren());
                    treeItem.setParent(newTreeItem.getParent());
                    treeItem.setTags(newTreeItem.getTags());
                    return repository.save(treeItem);
                })
                .orElseGet(() -> {
                    newTreeItem.setId(id);
                    return repository.save(newTreeItem);
                });
    }

    @DeleteMapping("/api/treeitem/{id}")
    void deleteTreeItem(@PathVariable Integer id) {
        repository.deleteById(id);
    }
}