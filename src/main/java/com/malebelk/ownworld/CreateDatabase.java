package com.malebelk.ownworld;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class CreateDatabase {

    @Bean
    CommandLineRunner initDatabase(TreeItemRepository repository) {
        return args -> {
            //log.info("Preloading " + repository.save(new TreeItem("root", "root")));
        };
    }
}
