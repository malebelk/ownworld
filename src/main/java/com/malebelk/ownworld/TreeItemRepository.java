package com.malebelk.ownworld;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Set;

public interface TreeItemRepository extends JpaRepository<TreeItem, Integer> {
    TreeItem getOneByParentNull();
    Set<TreeItem> findByParentId(Integer id);
}
