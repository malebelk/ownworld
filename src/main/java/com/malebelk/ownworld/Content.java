package com.malebelk.ownworld;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Data
@Entity
class Content {
    @Id
    private Integer id;

    @OneToOne
    private TreeItem treeItem;

    private String content;
}
