package com.malebelk.ownworld;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Set;
import javax.persistence.Id;

@Data
@Entity
class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    @ManyToMany
    private Set<TreeItem> listItems;
}
