package com.malebelk.ownworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OwnworldApplication {

	public static void main(String[] args) {
		SpringApplication.run(OwnworldApplication.class, args);
	}

}
