package com.malebelk.ownworld;

public class TreeItemNotFoundException extends Exception {
    public TreeItemNotFoundException(Integer id) {
        super("Could not find tree item " + id);
    }
}
