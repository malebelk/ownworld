package com.malebelk.ownworld;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
class TreeItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String name;

    private String picture;

    @OneToOne
    private Content content;

    @OneToMany(
            //cascade = {CascadeType.ALL},
            orphanRemoval = true,
            fetch = FetchType.LAZY
    )
    @JoinColumn(name = "parent_id")
   // @JsonIgnore
    private Set<TreeItem> children;

    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    //@JsonBackReference
    private TreeItem parent;

    @ManyToMany
    private Set<Tag> tags;

    TreeItem(String name, String picture) {
        this.name = name;
        this.picture = picture;
    }

    TreeItem(Integer id) {
        this.id = id;
    }
}